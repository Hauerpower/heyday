
export default function menuInit(){
	
	(function($){

	var menu_breakpoint = 'large';
	
	function fixedNav(){
		
		if( ! $('body').hasClass('fixed-nav') ) return;
		
		var scrolltop = $(window).scrollTop();
		var hTop = 0;
		
	
		if(scrolltop > hTop && !$('body').hasClass('scrolled') ){
			$('body').addClass('scrolled');
		}
		if(scrolltop <= hTop && $('body').hasClass('scrolled') ){
			$('body').removeClass('scrolled');
		}
			
	}
	
	function menuEvents(){
		
		$('#site-header .menu li.menu-item-has-children').append('<div class="arrow">');

		$('body').on('click', '#site-header .menu li.menu-item-has-children .arrow', function () {
			$(this).siblings('.sub-menu').stop().slideToggle(200);
		});


		$('body').on('click', '#site-header .menu li.menu-item-has-children > a[href="#"]', function () {
			if( ! Foundation.MediaQuery.atLeast(menu_breakpoint) ){
				$(this).siblings('.sub-menu').stop().slideToggle(200);
			}
		});
		
		$('body').on('click', '#site-header .menu .menu a:not([href="#"])', function () {
			if( ! Foundation.MediaQuery.atLeast(menu_breakpoint) ){
				$('#site-header .menu-wrap').removeClass('active');
			}
		});
		
		$('body').on('click', '#hamburger', function () {
			if( ! Foundation.MediaQuery.atLeast(menu_breakpoint) ){
				$('#site-header .menu-wrap').toggleClass('active');
			}

		});
		
		$('#site-header .menu li.menu-item-has-children').hover(function(){
			if( Foundation.MediaQuery.atLeast(menu_breakpoint) ){
				$(this).children('.sub-menu').stop().fadeIn(200);
			}
		}, function(){
			if( Foundation.MediaQuery.atLeast(menu_breakpoint) ){
				$(this).children('.sub-menu').stop().fadeOut(200);
			}
		});

	
		
		$(window).on('changed.zf.mediaquery', function() {
		  if(Foundation.MediaQuery.atLeast('large')){
				$('#site-header .menu .sub-menu').hide();
			}
		});
		
	}
	
	fixedNav();
	menuEvents();
	
	$(window).scroll(function(){
		requestAnimationFrame(function(){
			fixedNav();
		});
	});
	 
	})(jQuery);
	
}